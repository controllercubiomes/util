package util

type Sort struct {
	Field     string `json:"field"`
	Direction string `json:"direction"`
}

type Request struct {
	Filter string `json:"filter"`
	Sort   []Sort `json:"sort"`
}

const (
	TOKEN_NAME     = iota
	TOKEN_OPPERAND = iota
	TOKEN_VALUE    = iota
)

type Token struct {
	Token int    `json:"token"`
	Val   string `json:"val"`
}

type Chunk struct {
	ID          int    `json:"id"`
	Start       int64  `json:"start"`
	End         int64  `json:"end"`
	RadiusInner int    `json:"radius_inner"`
	RadiusOuter int    `json:"radius_outer"`
	ProgramURL  string `json:"program_url"`
	SeedsFound  int    `json:"seeds_found"`
	Key         string `json:"key"`
}

type AnalyzeChunk struct {
	Seeds       []int64 `json:"seeds"`
	RadiusInner int     `json:"radius_inner"`
	RadiusOuter int     `json:"radius_outer"`
	ProgramURL  string  `json:"program_url"`
}

type Initial struct {
	Finder   string `json:"finder"`
	Seed     int64  `json:"seed"`
	Criteria string `json:"criteria"`
}

type Seed struct {
	Finder              string    `json:"finder"`
	Seed                int64     `json:"seed"`
	Spawn_biome         string    `json:"spawn_biome"`
	Village_distance    int       `json:"village_distance"`
	GINI                float32   `json:"GINI"`
	Huts                int       `json:"huts"`
	Monuments           int       `json:"monuments"`
	Smallest_biome_name string    `json:"smallest_biome_name"`
	Smallest_biome      float32   `json:"smallest_biome"`
	Largest_biome_name  string    `json:"largest_biome_name"`
	Largest_biome       float32   `json:"largest_biome"`
	Ocean               Ocean     `json:"ocean"`
	Mesa                Mesa      `json:"mesa"`
	Desert              Desert    `json:"desert"`
	Savanna             Savanna   `json:"savanna"`
	Plains              Plains    `json:"plains"`
	Swamp               Swamp     `json:"swamp"`
	Forest              Forest    `json:"forest"`
	Roofed              Roofed    `json:"roofed"`
	Mountain            Mountain  `json:"mountain"`
	Mushroom            Mushroom  `json:"mushroom"`
	Jungle              Jungle    `json:"jungle"`
	Taiga               Taiga     `json:"taiga"`
	MegaTaiga           MegaTaiga `json:"mega_taiga"`
	Snowy               Snowy     `json:"snowy"`
	Basalt_deltas       float32   `json:"basalt_deltas"`
	Beach               float32   `json:"beach"`
	Crimson_forest      float32   `json:"crimson_forest"`
	End_barrens         float32   `json:"end_barrens"`
	End_highlands       float32   `json:"end_highlands"`
	End_midlands        float32   `json:"end_midlands"`
	Nether_wastes       float32   `json:"nether_wastes"`
	River               float32   `json:"river"`
	Small_end_islands   float32   `json:"small_end_islands"`
	Soul_sand_valley    float32   `json:"soul_sand_valley"`
	Stone_shore         float32   `json:"stone_shore"`
	The_end             float32   `json:"the_end"`
	The_void            float32   `json:"the_void"`
	Warped_forest       float32   `json:"warped_forest"`
}

type Ocean struct {
	Total               float32 `json:"total"`
	Cold_ocean          float32 `json:"cold_ocean"`
	Deep_cold_ocean     float32 `json:"deep_cold_ocean"`
	Deep_frozen_ocean   float32 `json:"deep_frozen_ocean"`
	Deep_lukewarm_ocean float32 `json:"deep_lukewarm_ocean"`
	Deep_ocean          float32 `json:"deep_ocean"`
	Deep_warm_ocean     float32 `json:"deep_warm_ocean"`
	Frozen_ocean        float32 `json:"frozen_ocean"`
	Frozen_river        float32 `json:"frozen_river"`
	Lukewarm_ocean      float32 `json:"lukewarm_ocean"`
	Ocean               float32 `json:"ocean"`
	Warm_ocean          float32 `json:"warm_ocean"`
}

type Mesa struct {
	Total                            float32 `json:"total"`
	Badlands                         float32 `json:"badlands"`
	Badlands_plateau                 float32 `json:"badlands_plateau"`
	Eroded_badlands                  float32 `json:"eroded_badlands"`
	Modified_badlands_plateau        float32 `json:"modified_badlands_plateau"`
	Modified_wooded_badlands_plateau float32 `json:"modified_wooded_badlands_plateau"`
	Wooded_badlands_plateau          float32 `json:"wooded_badlands_plateau"`
}

type Desert struct {
	Total        float32 `json:"total"`
	Desert       float32 `json:"desert"`
	Desert_hills float32 `json:"desert_hills"`
	Desert_lakes float32 `json:"desert_lakes"`
}

type Savanna struct {
	Total                     float32 `json:"total"`
	Savanna                   float32 `json:"savanna"`
	Savanna_plateau           float32 `json:"savanna_plateau"`
	Shattered_savanna         float32 `json:"shattered_savanna"`
	Shattered_savanna_plateau float32 `json:"shattered_savanna_plateau"`
}

type Plains struct {
	Total            float32 `json:"total"`
	Plains           float32 `json:"plains"`
	Sunflower_plains float32 `json:"sunflower_plains"`
}

type Swamp struct {
	Total       float32 `json:"total"`
	Swamp       float32 `json:"swamp"`
	Swamp_hills float32 `json:"swamp_hills"`
}

type Forest struct {
	Total              float32 `json:"total"`
	Birch_forest       float32 `json:"birch_forest"`
	Birch_forest_hills float32 `json:"birch_forest_hills"`
	Flower_forest      float32 `json:"flower_forest"`
	Forest             float32 `json:"forest"`
	Tall_birch_forest  float32 `json:"tall_birch_forest"`
	Tall_birch_hills   float32 `json:"tall_birch_hills"`
	Wooded_hills       float32 `json:"wooded_hills"`
}

type Roofed struct {
	Total             float32 `json:"total"`
	Dark_forest       float32 `json:"dark_forest"`
	Dark_forest_hills float32 `json:"dark_forest_hills"`
}

type Mountain struct {
	Total                       float32 `json:"total"`
	Gravelly_mountains          float32 `json:"gravelly_mountains"`
	Modified_gravelly_mountains float32 `json:"modified_gravelly_mountains"`
	Mountain_edge               float32 `json:"mountain_edge"`
	Mountains                   float32 `json:"mountains"`
	Wooded_mountains            float32 `json:"wooded_mountains"`
}

type Mushroom struct {
	Total                float32 `json:"total"`
	Mushroom_fields      float32 `json:"mushroom_fields"`
	Mushroom_field_shore float32 `json:"mushroom_field_shore"`
}

type Jungle struct {
	Total                float32 `json:"total"`
	Bamboo_jungle        float32 `json:"bamboo_jungle"`
	Bamboo_jungle_hills  float32 `json:"bamboo_jungle_hills"`
	Jungle               float32 `json:"jungle"`
	Jungle_edge          float32 `json:"jungle_edge"`
	Jungle_hills         float32 `json:"jungle_hills"`
	Modified_jungle      float32 `json:"modified_jungle"`
	Modified_jungle_edge float32 `json:"modified_jungle_edge"`
}

type Taiga struct {
	Total                 float32 `json:"total"`
	Snowy_taiga           float32 `json:"snowy_taiga"`
	Snowy_taiga_hills     float32 `json:"snowy_taiga_hills"`
	Snowy_taiga_mountains float32 `json:"snowy_taiga_mountains"`
	Taiga                 float32 `json:"taiga"`
	Taiga_hills           float32 `json:"taiga_hills"`
	Taiga_mountains       float32 `json:"taiga_mountains"`
}

type MegaTaiga struct {
	Total                    float32 `json:"total"`
	Giant_spruce_taiga       float32 `json:"giant_spruce_taiga"`
	Giant_spruce_taiga_hills float32 `json:"giant_spruce_taiga_hills"`
	Giant_tree_taiga         float32 `json:"giant_tree_taiga"`
	Giant_tree_taiga_hills   float32 `json:"giant_tree_taiga_hills"`
}

type Snowy struct {
	Total                 float32 `json:"total"`
	Ice_spikes            float32 `json:"ice_spikes"`
	Snowy_beach           float32 `json:"snowy_beach"`
	Snowy_mountains       float32 `json:"snowy_mountains"`
	Snowy_taiga           float32 `json:"snowy_taiga"`
	Snowy_taiga_hills     float32 `json:"snowy_taiga_hills"`
	Snowy_taiga_mountains float32 `json:"snowy_taiga_mountains"`
	Snowy_tundra          float32 `json:"snowy_tundra"`
}

const (
	OCEAN         = iota
	PLAINS        = iota
	DESERT        = iota
	MOUNTAINS     = iota
	FOREST        = iota
	TAIGA         = iota
	SWAMP         = iota
	RIVER         = iota
	NETHER_WASTES = iota
	THE_END       = iota
	// 10
	FROZEN_OCEAN         = iota
	FROZEN_RIVER         = iota
	SNOWY_TUNDRA         = iota
	SNOWY_MOUNTAINS      = iota
	MUSHROOM_FIELDS      = iota
	MUSHROOM_FIELD_SHORE = iota
	BEACH                = iota
	DESERT_HILLS         = iota
	WOODED_HILLS         = iota
	TAIGA_HILLS          = iota
	// 20
	MOUNTAIN_EDGE      = iota
	JUNGLE             = iota
	JUNGLE_HILLS       = iota
	JUNGLE_EDGE        = iota
	DEEP_OCEAN         = iota
	STONE_SHORE        = iota
	SNOWY_BEACH        = iota
	BIRCH_FOREST       = iota
	BIRCH_FOREST_HILLS = iota
	DARK_FOREST        = iota
	// 30
	SNOWY_TAIGA             = iota
	SNOWY_TAIGA_HILLS       = iota
	GIANT_TREE_TAIGA        = iota
	GIANT_TREE_TAIGA_HILLS  = iota
	WOODED_MOUNTAINS        = iota
	SAVANNA                 = iota
	SAVANNA_PLATEAU         = iota
	BADLANDS                = iota
	WOODED_BADLANDS_PLATEAU = iota
	BADLANDS_PLATEAU        = iota
	// 40  --  1.13
	SMALL_END_ISLANDS   = iota
	END_MIDLANDS        = iota
	END_HIGHLANDS       = iota
	END_BARRENS         = iota
	WARM_OCEAN          = iota
	LUKEWARM_OCEAN      = iota
	COLD_OCEAN          = iota
	DEEP_WARM_OCEAN     = iota
	DEEP_LUKEWARM_OCEAN = iota
	DEEP_COLD_OCEAN     = iota
	// 50
	DEEP_FROZEN_OCEAN = iota

	// ALIASES
	EXTREMEHILLS        = MOUNTAINS
	SWAMPLAND           = SWAMP
	HELL                = NETHER_WASTES
	SKY                 = THE_END
	FROZENOCEAN         = FROZEN_OCEAN
	FROZENRIVER         = FROZEN_RIVER
	ICEPLAINS           = SNOWY_TUNDRA
	ICEMOUNTAINS        = SNOWY_MOUNTAINS
	MUSHROOMISLAND      = MUSHROOM_FIELDS
	MUSHROOMISLANDSHORE = MUSHROOM_FIELD_SHORE
	DESERTHILLS         = DESERT_HILLS
	FORESTHILLS         = WOODED_HILLS
	TAIGAHILLS          = TAIGA_HILLS
	EXTREMEHILLSEDGE    = MOUNTAIN_EDGE
	JUNGLEHILLS         = JUNGLE_HILLS
	JUNGLEEDGE          = JUNGLE_EDGE
	DEEPOCEAN           = DEEP_OCEAN
	STONEBEACH          = STONE_SHORE
	COLDBEACH           = SNOWY_BEACH
	BIRCHFOREST         = BIRCH_FOREST
	BIRCHFORESTHILLS    = BIRCH_FOREST_HILLS
	ROOFEDFOREST        = DARK_FOREST
	COLDTAIGA           = SNOWY_TAIGA
	COLDTAIGAHILLS      = SNOWY_TAIGA_HILLS
	MEGATAIGA           = GIANT_TREE_TAIGA
	MEGATAIGAHILLS      = GIANT_TREE_TAIGA_HILLS
	EXTREMEHILLSPLUS    = WOODED_MOUNTAINS
	SAVANNAPLATEAU      = SAVANNA_PLATEAU
	MESA                = BADLANDS
	MESAPLATEAU_F       = WOODED_BADLANDS_PLATEAU
	MESAPLATEAU         = BADLANDS_PLATEAU
	WARMOCEAN           = WARM_OCEAN
	LUKEWARMOCEAN       = LUKEWARM_OCEAN
	COLDOCEAN           = COLD_OCEAN
	WARMDEEPOCEAN       = DEEP_WARM_OCEAN
	LUKEWARMDEEPOCEAN   = DEEP_LUKEWARM_OCEAN
	COLDDEEPOCEAN       = DEEP_COLD_OCEAN
	FROZENDEEPOCEAN     = DEEP_FROZEN_OCEAN

	THE_VOID = 127

	// MUTATED VARIANTS
	SUNFLOWER_PLAINS                 = PLAINS + 128
	DESERT_LAKES                     = DESERT + 128
	GRAVELLY_MOUNTAINS               = MOUNTAINS + 128
	FLOWER_FOREST                    = FOREST + 128
	TAIGA_MOUNTAINS                  = TAIGA + 128
	SWAMP_HILLS                      = SWAMP + 128
	ICE_SPIKES                       = SNOWY_TUNDRA + 128
	MODIFIED_JUNGLE                  = JUNGLE + 128
	MODIFIED_JUNGLE_EDGE             = JUNGLE_EDGE + 128
	TALL_BIRCH_FOREST                = BIRCH_FOREST + 128
	TALL_BIRCH_HILLS                 = BIRCH_FOREST_HILLS + 128
	DARK_FOREST_HILLS                = DARK_FOREST + 128
	SNOWY_TAIGA_MOUNTAINS            = SNOWY_TAIGA + 128
	GIANT_SPRUCE_TAIGA               = GIANT_TREE_TAIGA + 128
	GIANT_SPRUCE_TAIGA_HILLS         = GIANT_TREE_TAIGA_HILLS + 128
	MODIFIED_GRAVELLY_MOUNTAINS      = WOODED_MOUNTAINS + 128
	SHATTERED_SAVANNA                = SAVANNA + 128
	SHATTERED_SAVANNA_PLATEAU        = SAVANNA_PLATEAU + 128
	ERODED_BADLANDS                  = BADLANDS + 128
	MODIFIED_WOODED_BADLANDS_PLATEAU = WOODED_BADLANDS_PLATEAU + 128
	MODIFIED_BADLANDS_PLATEAU        = BADLANDS_PLATEAU + 128
	// 1.14
	BAMBOO_JUNGLE       = 168
	BAMBOO_JUNGLE_HILLS = 169
	// 1.16
	SOUL_SAND_VALLEY = 170
	CRIMSON_FOREST   = 171
	WARPED_FOREST    = 172
	BASALT_DELTAS    = 173
)
